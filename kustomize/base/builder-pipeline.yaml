# This pipeline builds a container image from a Git commit sha.
# Pushes the images to container registry and tag it with a short
# git commit sha and branch name.
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: joblinks-builder-pipeline
spec:
  workspaces:
  - name: shared-workspace
  - name: dummy-workspace
  # Need for dummy-workspace shall hopefully be removed, once git-cli and skopeo-copy support optional ws.
  params:
  - name: git-repo-url
    type: string
    description: url of the git repo for the code
  - name: gitlab-repository-full-name
    type: string
    description: |
      The GitLab repository full name, e.g.: arbetsformedlingen/import-to-elastic
  - name: git-branch-name
    type: string
    description: branch to build (only used to tag image)
  - name: git-commit
    description: sha of commit to build
  - name: git-commit-short
    description: short sha of commit, used as tag for images
  - name: image
    type: string
    description: |
      image to be build from the code, excluding tag. Image tag is deducted from sha and branch.
  - name: ocs-ui-base-url
    type: string
    description: Base URL to OCS admin interface.
    default: "https://console-openshift-console.test.services.jtech.se"
  tasks:
  - name: gitlab-run-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/joblinks-build"
    - name: STATE
      value: running
    - name: DESCRIPTION
      value: Build has been trigged and is building.
  - name: fetch-repository
    taskRef:
      name: git-clone
      kind: ClusterTask
    workspaces:
    - name: output
      workspace: shared-workspace
    params:
    - name: url
      value: $(params.git-repo-url)
    - name: subdirectory
      value: ""
    - name: deleteExisting
      value: "true"
    - name: revision
      value: $(params.git-commit)
    runAfter:
    - gitlab-run-status
  - name: build-image
    taskRef:
      name: buildah-v0-16-3
      kind: ClusterTask
    params:
    - name: IMAGE
      value: $(params.image):$(params.git-commit-short)
    workspaces:
    - name: source
      workspace: shared-workspace
    runAfter:
    - fetch-repository
  - name: tag-image-with-branch
    taskRef:
      name: skopeo-copy
      kind: Task
    params:
    - name: srcImageURL
      value: docker://$(params.image):$(params.git-commit-short)
    - name: destImageURL
      value: docker://$(params.image):$(params.git-branch-name)
    workspaces:
    - name: images-url
      workspace: dummy-workspace
    runAfter:
    - build-image
  - name: tag-image-latest-on-main-branch
    when:
    - input: "$(params.git-branch-name)"
      operator: in
      values:
      - master
      - main
    taskRef:
      name: skopeo-copy
      kind: Task
    params:
    - name: srcImageURL
      value: docker://$(params.image):$(params.git-commit-short)
    - name: destImageURL
      value: docker://$(params.image):latest
    workspaces:
    - name: images-url
      workspace: dummy-workspace
    runAfter:
    - build-image
  finally:
  - name: gitlab-success-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Succeeded"
      - "Completed"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/joblinks-build"
    - name: STATE
      value: success
    - name: DESCRIPTION
      value: Build has been finalized with success.
  - name: gitlab-failed-status
    taskRef:
      name: jobtech-gitlab-set-status
      kind: ClusterTask
    when:
    - input: "$(tasks.status)"
      operator: in
      values:
      - "Failed"
      - "None"
    params:
    - name: REPO_FULL_NAME
      value: $(params.gitlab-repository-full-name)
    - name: GITLAB_TOKEN_SECRET_NAME
      value: gitlab-access
    - name: GITLAB_TOKEN_SECRET_KEY
      value: username
    - name: SHA
      value: $(params.git-commit)
    - name: TARGET_URL
      value: "$(params.ocs-ui-base-url)/k8s/ns/$(context.pipelineRun.namespace)/tekton.dev~v1beta1~PipelineRun/$(context.pipelineRun.name)"
    - name: CONTEXT
      value: "ci/joblinks-build"
    - name: STATE
      value: failed
    - name: DESCRIPTION
      value: Build failed. Check logs for details.
