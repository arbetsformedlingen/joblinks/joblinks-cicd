# Install Joblinks CICD

This describes how to install Joblinks-cicd on a Kubernetes/OpenShift cluster.
Prerequisites is that [Tekton](https:/(/tekton.dev/)) is installed on the cluster,
in OpenShift this is the OpenShift Pipeline Operator.

First create a project(namespace in plain Kubernetes):

```shell
oc new project joblinks-cicd
```

Take the files in [examples directory](examples) and modify them with your
organizations secrets.

* [docker-registry-secret-example.yaml](examples/docker-registry-secret-example.yaml) contains login for the Docker registry the container images are pushed too.
* [gitlab-access-token-example.yaml](examples/gitlab-access-token-example.yaml) contains the
token for the user with the rights to read and write to all projects.
* [gitlab-webhook-secret.yaml](examples/gitlab-webhook-secret.yaml) contains the secret
sent together with the webhooks from GitLab to Joblinks CICD to trigger builds etc. You shall
share this with all users that shall be able to set up project to use Joblinks CICD.

Once you have modified the files with secrets apply them one by one using
`kubectl apply -f FILENAME`

Deploy the rest of Joblinks CICD:
 `kubectl apply -k kustomize/overlays/prod`


After the installation you can get the webhook-url via:

```shell
kubectl -n joblinks-cicd-prod get route el-joblinks-cicd-listener -o jsonpath="https://{.spec.host}"
```

Share this URL and the webhook secret with your users of Joblinks CICDs.
These are used when setting up webhooks in GitLab.

The address to the webhook is shown via `kubectl get route`
